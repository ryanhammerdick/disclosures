## Overview

A Server Side Request Forgery (SSRF) vulnerability in go-camo up to version 1.1.4 allows a remote attacker to perform HTTP requests to internal endpoints.

## Affected Products

- [go-camo](https://github.com/cactus/go-camo) up to version 1.1.4

## Disclosure Timeline

- 2019-07-23 Initial report to the maintainer of Go-Camo project
- 2019-07-26 Go-Camo v1.1.5 released with fixes

## CVE Identifier

- [CVE-2019-14255](https://nvd.nist.gov/vuln/detail/CVE-2019-14255)

## Details

### Improper handling of filtered request types

While requests that were not GET/HEAD received a reply 405 `Method not allowed`
response, an outgoing request was still being improperly performed. The request flow was as follows:


```mermaid
sequenceDiagram
    Client->>GoCamo: POST /<image url>/<hmac>
    GoCamo->>Client: 405 Method not allowed
    GoCamo->>Host: POST <image url>
```

This behavior could have been used for a blind SSRF if an attacker managed to bypass the IP blacklist functionality (see next section).

### No validation of redirects with regard to blacklists/whitelists

Redirect URLs were not being validated against the blacklists/whitelists used by Go-Camo.

This meant that redirects such as these were being followed:

In consequence, Go-Camo would follow a redirect such as:

```
HTTP/1.1 307 Temporary Redirect
Location: http://localhost:3333
...
```

In combination with the aforementioned improper request filtering, this allowed an attacker to send a request with arbitrary HTTP method to internal endpoints.

```mermaid
sequenceDiagram
    Client->>GoCamo: POST /<image url>/<hmac>
    GoCamo->>Client: 405 Method not allowed
    GoCamo->>Host: POST <image url>
    Host->>GoCamo: 307 Location: http://internal_ip:3333
    GoCamo->>internal_ip: POST http://internal_ip:3333
```

## Impact

A remote attacker can use the described bugs to make Go-Camo send HTTP requests to internal endpoints. The attacker can control the HTTP method and the queried URL that is requested by Go-Camo.

## Mitigation

Update to Go-Camo version 1.1.5

## External References

- [Go-camo advisory](https://github.com/cactus/go-camo/security/advisories/GHSA-xrmp-4542-q746)

## Credits

Dennis Appelt - GitLab Security Research Team