## Summary

Go-Camo's build-in check to only proxy resources of type `image/*` (and optionally `video/*` and `audio/*`) can be bypassed. Go-Camo can be tricked to proxy resources of arbitrary `Content-Type`.

## Affected Products

- [go-camo](https://github.com/cactus/go-camo) before version 2.1.1

## Disclosure Timeline

- 2019-11-08 Initial report to the go-camo project via GitHub 
- 2019-11-12 Go-camo v2.1.1 released with fixes

## CVE Identifier

- [CVE-2019-18923](https://nvd.nist.gov/vuln/detail/CVE-2019-18923)

## Details

Go-Camo is supposed to only proxy media files. If a resource is fetched that is not an image or video, it should refuse to pass the resource along to the client.

You can try this out for yourself on https://gitlab.com by creating a link to a resource that is not an image. For example, the markdown `![not an image](https://about.gitlab.com)` is rendered as:

![not an image](https://about.gitlab.com)

If you click on *not an image* you will see go-camo replies with an error message.

What happens behind the scenes is that Go-Camo fetched the image source `https://about.gitlab.com` and noticed that the content type of this resource is `text/html`. Because the returned content type does not match `image/*` or `video/*`, Go-Camo returns the error message `Unsupported content-type returned`. This behavior is an important security precaution as it stops attackers from proxying arbitrary content through Go-Camo, which could lead to XSS or phishing pages being served by Camo.

Interestingly enough, the content type filter can be bypassed by providing multiple values for the header `Content-Type`. The problem is that Go-Camo only checks that the first value is of the expected type; the following values are not validated. For example, `Content-Type: image/png, text/html; charset=UTF-8` will bypass Camo's content type validation, because `image/png` matches `image/*`.

The code that does the content type validation relies on globbing and, hence, the whitelist image/* matches the content type from the previous example. This is the [code](https://github.com/cactus/go-camo/blob/505862f7bf14c8b6ff945734d5f3fdcd929e45dd/pkg/camo/proxy.go#L453-L460):

```go
	// re-use the htrie glob path checker for accept types validation
	acceptTypesFilter := htrie.NewGlobPathChecker()
	for _, v := range acceptTypes {
		err := acceptTypesFilter.AddRule("|i|" + v)
		if err != nil {
			return nil, err
		}
	}
```

This bug in the content type validation would not matter much if browsers would be conservative when processing an ambiguous `Content-Type` header. But since this is the web, browsers strive for a maximum of compatibility. When provided with multiple content types, browsers seem to automatically choose the type that fits the resource. The following HTTP response will be rendered by Chrome as well as Firefox as `text/html`:

```
HTTP/1.1 200 OK
Content-Length: 292
Content-Security-Policy: media-src 'self'; script-src 'none'; object-src 'none'
Content-Type: image/svg+xml, text/html; charset=UTF-8
Date: Fri, 08 Nov 2019 17:15:43 GMT
Server: go-camo
X-Content-Type-Options: nosniff
X-Xss-Protection: 1; mode=block
Via: 1.1 google
Alt-Svc: clear
Connection: close

<html>
  <head>
    <title>test</title></head>
    <body>
      <!-- Insert arbitrary HTLM here -->
    </body>
</html>
```

## Impact

This bug allows to serve arbitary content via Go-Camo, which can lead to XSS, phishing pages being served by Camo and more.

## Mitigation

Update to Go-Camo 2.1.1

## External References

- [Go-camo advisory](https://github.com/cactus/go-camo/security/advisories/GHSA-jg2r-qf99-4wvr)

# Credits

Dennis Appelt - GitLab Security Research Team