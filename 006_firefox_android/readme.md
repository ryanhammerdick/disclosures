## Summary

The SSDP engine in Firefox for Android (68.11.0 and below) can be tricked into triggering Android intent URIs with zero user interaction.  This attack can be leveraged by attackers on the same WiFi network and manifests as applications on the target device suddenly launching, without the users' permission, and conducting activities allowed by the intent.

The SSDP functionality has been completely removed in versions 79 and beyond. Firefox desktop applications do not contain the vulnerable functionality.

## Affected Products

- [Firefox for Android](https://play.google.com/store/apps/details?id=org.mozilla.firefox) versions 68.11.0 and below.

## Disclosure Timeline

- 2020-08-16: Detailed report provided to Mozilla via https://bugzilla.mozilla.org.
- 2020-08-17: Mozilla confirms that version 79, which does not contain the vulnerable code, is currently being rolled out globally. Mozilla starts a process to ensure this functionality is not re-introduced at a later time.
- 2020-08-27: Version 79 available globally via Google Play and automatic updates.
- 2020-09-15: Public disclosure.

## CVE Identifier

No CVE at this time.

## Details

A thorough explanation of the vulnerability is covered in [this write up](https://gitlab.com/gitlab-com/gl-security/security-operations/gl-redteam/red-team-tech-notes/-/tree/master/firefox-android-2020).


## Impact

The vulnerability resembles RCE (remote command execution) in that a remote (on same WiFi network) attacker can trigger the device to perform unauthorized functions with zero interaction from the end user. However, that execution is not totally arbitrary in that it can only call predefined application intents.

Had it been used in the wild, it could have targeted known-vulnerable intents in other applications. Or, it could have been used in a way similar to phishing attacks where a malicious site is forced onto the target without their knowledge in the hopes they would enter some sensitive info or agree to install a malicious application.

## Mitigation

Update to the latest version of Firefox for Android using the Google Play Store.

## External References

- [Detailed blog posting](https://gitlab.com/gitlab-com/gl-security/security-operations/gl-redteam/red-team-tech-notes/-/tree/master/firefox-android-2020)
- [Initial bug report](https://bugzilla.mozilla.org/show_bug.cgi?id=1659381)

# Credits

Chris Moberly - GitLab Red Team