## Summary

Google Cloud Platform's [OS Login](https://cloud.google.com/compute/docs/oslogin/) provides two levels of authorization using the following IAM roles:

- [roles/compute.osLogin](https://cloud.google.com/compute/docs/access/iam#compute.osLogin), which does not grant administrator permissions
- [roles/compute.osAdminLogin](https://cloud.google.com/compute/docs/access/iam#compute.osAdminLogin), which grants administrator permissions.

Privilege escalation from the role of `compute.osLogin` to `compute.osAdminLogin` was possible via multiple methods due to overly permissive group memberships assigned to all users connecting via the SSH protocol.

## Affected Products

- [guest-oslogin](https://github.com/GoogleCloudPlatform/guest-oslogin/releases) before version 20200504.00.

## Disclosure Timeline

- 2020-03-03: Initial report of DHCP poisoning and metadata server hijacking using `adm` group
- 2020-03-03: Initial report of privilege escalation using `lxd` group
- 2020-04-10: Added a report to indicate `docker` group was vulnerable as well
- 2020-04-28: All three issues fixed via https://github.com/GoogleCloudPlatform/guest-oslogin/pull/29/files
- 2020-06-03: Google confirms that fixed have been rolled out to cloud images / repositories
- 2020-06-04: Public disclosure

## CVE Identifier

- [CVE-2020-8903](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-8903): privilege escalation via DHCP and metadata server hijacking
- [CVE-2020-8907](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-8907): privilege escalation via docker user group
- [CVE-2020-8933](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-8933): privilege escalation via lxd user group

## Details

A thorough explanation of each vulnerability is covered in [this write up](https://gitlab.com/gitlab-com/gl-security/gl-redteam/red-team-tech-notes/-/tree/master/oslogin-privesc-june-2020).


## Impact

Users who had already gained local access to a Linux compute instance via the low-privilege OS Login role could execute commands as root.

## Mitigation

All newly deployed compute instances will include the fixed packages, and no action is required. Existing instances should apply updates via their standard package managers.

Some instances with OS Login already enabled may need to edit their `/etc/security/group.conf` file to exclude these specific user groups from the `# Added by Google Compute Engine OS Login` section: `adm`, `lxd`, `docker`.

## External References

- [Detailed blog posting](https://gitlab.com/gitlab-com/gl-security/gl-redteam/red-team-tech-notes/-/tree/master/oslogin-privesc-june-2020)
- Official [Google Cloud Security Bulletin](https://cloud.google.com/compute/docs/security-bulletins#2020619)

# Credits

Chris Moberly - GitLab Red Team